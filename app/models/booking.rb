class Booking < ApplicationRecord
  # Relationships
  belongs_to :flight
  has_many :passenger_bookings
  has_many :passengers, through: :passenger_bookings

  accepts_nested_attributes_for :passengers,
    reject_if: lambda { |attributes| attributes['name'].blank? }

end

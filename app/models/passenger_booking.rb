class PassengerBooking < ApplicationRecord
  # Relationships
  belongs_to :passenger
  belongs_to :booking

  accepts_nested_attributes_for :passenger

end

class Flight < ApplicationRecord
  attr_reader :duration
  
  # Relationships
  belongs_to :airline
  belongs_to :departure_airport, class_name: 'Airport'
  belongs_to :destination_airport, class_name: 'Airport'
  has_many :bookings
  has_many :passengers, through: :bookings
  
  # Alias
  alias_attribute :from_airport, :departure_airport
  alias_attribute :to_airport,   :destination_airport
  
  # Return flight duration (string)
  def duration
    humanize (arrival - departure)
  end
  
  # Returns flight number with airline name
  def full_flight_number
    "#{airline.name} #{flight_number}"
  end
  
  # Returns flight number with airline short_code
  def short_flight_number
    "#{airline.short_code}#{flight_number}"
  end

  def self.available_dates
    flights = Flight.all.order(departure: :asc)
		flights.map { |flight| flight.departure.strftime("%Y/%m/%d") }.uniq
  end
  
  def self.search(search_params)
    d_date = Date.parse(search_params[:departure])
    Flight.where(departure_airport: search_params[:departure_airport],
                 destination_airport: search_params[:destination_airport],
                 departure: d_date.beginning_of_day..d_date.end_of_day)
  end
  
  private
  
    def humanize secs
      [[60, :seconds], [60, :minutes], [24, :hours], [1000, :days]].map{ |count, name|
        if secs > 0
          secs, n = secs.divmod(count)
          "#{n.to_i} #{name}"
        end
      }.compact.reverse.join(' ')
    end
end

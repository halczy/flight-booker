class FlightsController < ApplicationController
  before_action :set_flight, only: :show

  def index
    @airports_options = Airport.all.map { |ap| [ap.iata, ap.id] }
    @dates = Flight.available_dates

    if params[:commit]
      unless validate_params
        redirect_to root_url
        return
      end
      @flights = Flight.search(search_params)
    end

  end

  def show
  end

  private

    def search_params
      params.permit(:departure_airport, :destination_airport, :departure,
                    :num_passengers)
    end

    def validate_params
      case
      when params[:departure_airport].empty?
        flash[:warning] = "No departure airport selected."
        return false
      when params[:destination_airport].empty?
        flash[:warning] = "No destination airport selected."
        return false
      when params[:departure_airport] == params[:destination_airport]
        flash[:warning] = "Departure airport and destionation airport is the same."
        return false
      when params[:departure].empty?
        flash[:warning] = "Please select a departure date."
        return false
      else
        return true
      end
    end

    def set_flight
      @flight = Flight.find(params[:id])
    end

end

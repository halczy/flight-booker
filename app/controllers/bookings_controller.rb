class BookingsController < ApplicationController
  before_action :set_booking, only: [:show]
  def new
    @booking = Booking.new
    @flight = Flight.find(params[:flight])
    num_passengers = params[:num_passengers].to_i
    num_passengers.times { @booking.passengers.build }
  end

  def create
    @booking = Booking.create(booking_params)
    params[:booking][:passengers_attributes].each do |passenger|
      @booking.passengers.create(name: passenger["name"])
    end
    redirect_to @booking
  end

  def show
  end

  private

    def booking_params
      params.require(:booking).permit(:flight_id,
                                      passengers_attributes: [ :passenger_id, :name ])

    end

    def set_booking
      @booking = Booking.find(params[:id])
    end
end

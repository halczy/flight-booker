class CreateFlights < ActiveRecord::Migration[5.0]
  def change
    create_table :flights do |t|
      t.references :departure_airport, foreign_key: true
      t.references :destination_airport, foreign_key: true
      t.references :airline, foreign_key: true
      t.string :flight_number
      t.datetime :departure
      t.datetime :arrival

      t.timestamps
    end
  end
end

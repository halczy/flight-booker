# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Airport.destroy_all
Airline.destroy_all
Flight.destroy_all

Airport.create([
  {name: 'Hartsfield Jackson Atlanta International', city: 'Atlanta', iata: 'ATL' },
  {name: 'Beijing Capital International', city: 'Beijing', iata: 'PEK' },
  {name: 'Dubai International', city: 'Dubai', iata: 'DBX' },
  {name: 'Chicago O’Hare International', city: 'Chicago', iata: 'ORD' },
  {name: 'Tokyo International', city: 'Tokyo', iata: 'HND' },
  {name: 'London Heathrow', city: 'United Kingdom', iata: 'LHR' }
])

Airline.create([
  {name: 'American Airlines', short_code: 'AA'},
  {name: 'Delta Airlines', short_code: 'DL'},
  {name: 'China Southern Airlines', short_code: 'CZ'},
  {name: 'Virgin America', short_code: 'VX'}
])

Airline.all.each do |airline|
  50.times do |n|
    airports = Airport.all.ids
    d_date = Faker::Time.between(DateTime.now, DateTime.now + 0.4)
    a_date = Faker::Time.between(DateTime.now + 0.5, DateTime.now + 1)
    airline.flights.create(
      departure_airport_id: airports.shuffle!.pop,
      destination_airport_id: airports.shuffle!.pop,
      flight_number: Faker::Number.number(3),
      departure: d_date,
      arrival: a_date)
  end
end